with open('liczby1.txt', 'r') as f:
    octal = list(map(str.strip, f.readlines()))

with open('liczby2.txt', 'r') as f:
    decimal_raw = list(map(str.strip, f.readlines()))

# zad 1

biggest_index = lowest_index = 0
for i in range(len(octal)):
    number = int(octal[i], 8)
    if number > int(octal[biggest_index], 8):
        biggest_index = i
    elif number < int(octal[lowest_index], 8):
        lowest_index = i

print('lowest:', octal[lowest_index])
print('biggest:', octal[biggest_index])

# zad 2

decimal = list(map(int, decimal_raw))
current_series = biggest_series = [decimal[0]]
for i in range(1, len(decimal)):
    if decimal[i] >= current_series[-1]:
        current_series.append(decimal[i])
    else:
        if len(current_series) > len(biggest_series):
            biggest_series = current_series
        current_series = [decimal[i]]

print('series starts with:', biggest_series[0])
print('it has', len(biggest_series), 'elements')

same = bigger = 0
for (d, o) in zip(decimal, range(len(octal))):
    o = int(octal[o], 8)

    if d == o:
        same += 1
    elif o > d:
        bigger += 1

print('a:', same)
print('b:', bigger)


# zad 4

sum_dec = sum_oct = 0
for i in decimal_raw:
    sum_dec += i.count('6')
    to_oct = oct(int(i))
    sum_oct += to_oct.count('6')
print('number 6 appears', sum_dec, 'times in decimal')
print('number 6 appears', sum_oct, 'times in octal')
