#!/usr/bin/python3

import math as maths

with open('liczby.txt', 'r') as f:
    data = list(map(int, f.readlines()))


# zad 1
less_than_1000 = []
for i in data:
    if i < 1000:
        less_than_1000.append(i)
print('liczb mniejszych niz 1000:', len(less_than_1000))
print('dwie ostatnie liczby: {}, {}'.format(
    less_than_1000[-1], less_than_1000[-2]))

# zad 2


def denominators(number):
    denominator_list = []
    for i in range(1, number + 1):
        if number % i == 0:
            denominator_list.append(i)
    return denominator_list


denominator_dict = {}

# generate denominators
print('Generating all denominators...')
for i in data:
    denominator_dict[i] = denominators(i)
print('Generating finished')

for (k, v) in denominator_dict.items():
    if len(v) == 18:
        print('liczba: {}, dzielniki: {}'.format(k, v))


# zad 3

def indirectly_prime(a, b, denominator_store):
    a_divs = set(denominator_store[a])
    b_divs = set(denominator_store[b])

    return a_divs & b_divs == {1}


biggest = data[0]
for k in denominator_dict:
    if not indirectly_prime(biggest, k, denominator_dict):
        continue
    if k > biggest:
        biggest = k

print(biggest, denominator_dict[biggest])
