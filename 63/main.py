#!/usr/bin/python3

import math as maths

with open('ciagi.txt', 'r') as f:
    data = list(map(str.strip, f.readlines()))

# zad 1
print('ciagi dwucykliczne:')
for series in data:
    # middle is invalid when length is odd, but even then, both slices are different
    middle = len(series) // 2
    if series[:middle] == series[middle:]:
        print(series)

# zad 2
series_without_11 = 0
for series in data:
    if series.find('11') == -1:
        series_without_11 += 1

print('ciagi bez dwoch jedynek:', series_without_11)

# zad 3


def generate_primes(max):
    primes = [True for x in range(max)]
    for i in range(2, maths.ceil(maths.sqrt(max))):
        if primes[i] == True:
            j = i ** 2
            while j < max:
                primes[j] = False
                j += i

    primes2 = []
    for (i, prime) in enumerate(primes):
        if prime == True:
            primes2.append(i)

    return primes2[3:]


primes = generate_primes(1000000)


def is_semiprime(number, primes):
    for p in primes:
        if p > number:
            break
        else:
            quotient = number / p
            if int(quotient) == quotient and int(quotient) in primes:
                return True
    return False


how_many_semiprimes = 0
biggest = 0
smallest = float('Inf')
for number in data:
    number = int(number, 2)
    if is_semiprime(number, primes):
        how_many_semiprimes += 1
        if number > biggest:
            biggest = number
        elif number < smallest:
            smallest = number
        print(number, 'is semi')

print('there are', how_many_semiprimes, 'semiprime numbers')
print('biggest semiprime:', biggest)
print('smallest semiprime:', smallest)
