#!/usr/bin/python3

import math as maths

input_filename = "liczby.txt"
output_filename = "wyniki_liczby.txt"

data = []

with open(input_filename, 'r') as f:
    data = list(map(int, f.readlines()))

def generate_primes(max):
    primes = [True for x in range(max)]
    for i in range(2, maths.ceil(maths.sqrt(max))):
        if primes[i] == True:
            j = i ** 2
            while j < max:
                primes[j] = False
                j += i

    primes2 = []
    for (i, prime) in enumerate(primes):
        if prime == True:
            primes2.append(i)

    return primes2[3:]

primes = generate_primes(100000000)

def factorize(number, primes):
    factors = []
    while number > 1:
        denominator = lowest_denominator(number, primes)
        if denominator is None:
            break
        else:
            number //= denominator
            factors.append(denominator)
    return factors

def lowest_denominator(number, primes):
    for i in primes:
        if number % i == 0:
            return i

def has_three_distinct_odd_prime_denominators(number, primes):
    if number % 2 == 0:
        return False
    else:
        unique_denominators = set(factorize(number, primes))
        return len(unique_denominators) == 3

#zad 1
valid = 0
for number in data:
    if has_three_distinct_odd_prime_denominators(number, primes):
        valid += 1
print(valid)

# zad 2

palindromes = 0
for number in data:
    rev = int(str(number)[::-1])
    sum = number + rev
    if str(sum) == str(sum)[::-1]:
        palindromes += 1
print(palindromes)

# zad 3
def power(number, counter = 0):
    number = str(number)
    if len(number) == 1:
        return counter
    else:
        product = int(number[0])
        for digit in number[1:]:
            product *= int(digit)
        return power(product, counter=counter+1)

powers = {}
powers['max'] = data[0]
powers['min'] = data[0]

for i in range(1, 9):
    powers[i] = 0

for number in data:
    p = power(number)
    if p in range(1, 9):
        powers[p] += 1
        if p == 1:
            if number < powers['min']:
                powers['min'] = number
            elif number > powers['max']:
                powers['max'] = number
print(powers)
