#!/usr/bin/python3
import math as maths


def distance(a, b):
    return maths.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)


with open('Dane_PR2/punkty.txt', 'r') as f:
    lines = f.readlines()
    raw = list(map(str.split, lines))
    data = []
    for pair in raw:
        pair = tuple(map(int, pair))
        data.append(pair)

# 4.1
inside = 0
for point in data:
    d = distance((200, 200), point)
    if d == 200.0:
        print(point)
    elif d < 200.0:
        inside += 1
print(inside)

# 4.2


def pi_approx(data, n):
    inside = 0
    for point in data[:n]:
        if distance((200, 200), point) <= 200.0:
            inside += 1
    return (inside / len(data[:n])) * 4


# print(pi_approx(data, 1000))
# print(pi_approx(data, 5000))
# print(pi_approx(data, 100))
