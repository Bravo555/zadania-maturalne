def save_to_file(filename, text):
    with open(filename, 'w') as f:
        f.write(text)


def save_to_file_callback(filename, callback):
    with open(filename, 'w') as f:
        f.write(callback())


def load_series(filename):
    with open(filename, 'r') as f:
        a = f.readlines()

    b = []
    for i in range(len(a) // 2):
        b.append(a[2 * i + 1])

    b = list(map(str.split, b))
    data = []
    for i in b:
        data.append(list(map(int, i)))
    return data


# zad 1

data = load_series('ciagi.txt')


def is_valid_arithmetic_series(series):
    r = series[1] - series[0]
    prev = series[1]
    for an in series[2:]:
        if an - prev != r:
            return False
        prev = an
    return r


def zad1():
    output = ''
    how_many_art = 0
    biggest_diff = 0
    for series in data:
        diff = is_valid_arithmetic_series(series)
        if diff != False:
            how_many_art += 1
            if diff > biggest_diff:
                biggest_diff = diff

    output += 'Najwieksza roznica: {}\n'.format(biggest_diff)
    output += 'W danych jest {} ciagow arytmetycznych\n'.format(how_many_art)
    return output


save_to_file_callback('wynik.txt', zad1)

# zad 2


def is_cubed_natural_number(number):
    for i in range(number):
        cubed = i ** 3
        if cubed == number:
            return True
        elif cubed > number:
            return False


output = ''
for series in data:
    biggest = None
    for elem in series:
        if is_cubed_natural_number(elem):
            if biggest == None:
                biggest = elem
            elif elem > biggest:
                biggest = elem
    output += '{}\n'.format(biggest if biggest != None else 'nie ma takiego')
save_to_file('wynik2.txt', output)

# zad 3


def find_invalid_element(series):
    differences = []
    for i in range(len(series) - 1):
        r = series[i+1] - series[i]
        differences.append(r)
    # invalid element can appear in the beginning, middle, and end
    if differences[0] != differences[1] and differences[1] == differences[2]:
        return series[0]
    elif differences[-1] != differences[-2] and differences[-2] == differences[-3]:
        return series[-1]
    else:
        for i in range(1, len(differences) - 1):
            if differences[i] != differences[i-1] and differences[i] != differences[i+1]:
                return series[i+1]


def zad3():
    data = load_series('bledne.txt')
    output = ''
    for series in data:
        invalid = find_invalid_element(series)
        output += '{}\n'.format(invalid)
    return output


save_to_file_callback('wynik3.txt', zad3)
